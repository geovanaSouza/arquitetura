# Arquitetura

Este projeto trata-se da criação via código da diagramação da arquitetura implementada para a disciplina de Cloud Computing & DevSecOps do curso de pós graduação em engenharia Devops da Universidade Positivo em Curitiba/PR.

## Pré-requisitos

Utilizamos a biblioteca diagrams do python (https://diagrams.mingrammer.com/) para geração do diagrama via código. Para codificação e geração dos diagramas o seguinte pré-requisito deve ser atendido:

* Python 3.6 ou superior

* Biblioteca de SO: graphviz (Instruções de instalação em https://graphviz.gitlab.io/download/)

## Diagrama

![Arquitetura Aplicação](https://bitbucket.org/geovanaSouza/arquitetura/downloads/application.png)
