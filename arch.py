# diagram.py
from diagrams import Diagram
from diagrams.gcp.network import LoadBalancing
from diagrams.gcp.storage import GCS
from diagrams.gcp.network import DNS
from diagrams.gcp.compute import GCF
from diagrams.gcp.database import SQL

with Diagram("Application", show=False):
    DNS("up.geovanapossenti.com") >> LoadBalancing("LoadBalance") >> GCS("bucket") >> GCF("Backend") >> SQL("PostgreSQL")
